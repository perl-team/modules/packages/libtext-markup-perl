libtext-markup-perl (0.33-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.33.
  * Refresh (test) dependencies.

 -- gregor herrmann <gregoa@debian.org>  Sun, 03 Mar 2024 15:22:55 +0100

libtext-markup-perl (0.32-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.32.
  * Update years of upstream copyright.
  * Add test dependency on libcommonmark-perl.
  * Add test dependencies and suggestions on asciidoc and asciidoctor.

 -- gregor herrmann <gregoa@debian.org>  Sat, 10 Feb 2024 02:18:06 +0100

libtext-markup-perl (0.31-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.31.
  * d/copyright: bump upstream copyright years.
  * Remove all patches: applied upstream.
  * d/control: depend on libcommonmark-perl.

 -- Étienne Mollier <emollier@debian.org>  Tue, 21 Nov 2023 21:27:38 +0100

libtext-markup-perl (0.24-2) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit, Repository, Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ gregor herrmann ]
  * Cherry-pick upstream commit to fix test with docutils 0.20.
    (Closes: #1042643)
  * Declare compliance with Debian Policy 4.6.2.
  * Set Rules-Requires-Root: no.
  * Bump debhelper-compat to 13.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

 -- gregor herrmann <gregoa@debian.org>  Sun, 29 Oct 2023 18:21:57 +0100

libtext-markup-perl (0.24-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.24.
  * Drop patches, both merged upstream.
    With one small exception:
  * Fix hashbang of python script Text/Markup/rst2html_lenient.py in
    debian/rules.
  * Update years of upstream copyright.

 -- gregor herrmann <gregoa@debian.org>  Sat, 14 Dec 2019 03:04:21 +0100

libtext-markup-perl (0.23-4) unstable; urgency=medium

  * Team upload.
  * Convert code to python3:
    - Look for python3 in lib/Text/Markup/Rest.pm.
    - Port lib/Text/Markup/rst2html_lenient.py to python3.
    - Update Build-Depends-Indep and Recommends to python3.
    Closes: #943159
  * Annotate test-only build dependencies with <!nocheck>.
  * Declare compliance with Debian Policy 4.4.1.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.
  * Remove obsolete fields Name, Contact from debian/upstream/metadata.

 -- gregor herrmann <gregoa@debian.org>  Wed, 30 Oct 2019 20:42:35 +0100

libtext-markup-perl (0.23-3) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Use (only) autopkgtest-pkg-perl instead of the handcrafted
    (additional?) tests.
  * Declare compliance with Debian Policy 4.2.1.
  * Dropn unneeded version constraints from (build) dependencies.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Sat, 15 Dec 2018 19:39:13 +0100

libtext-markup-perl (0.23-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Fix spelling error for 'currently' in long description.
    Thanks to Alberto Luaces <aluaces@udc.es>

  [ Axel Beckert ]
  * Add patch to fix missing <head> in rendered output of many formats.
    (Closes: #793321)
  * Fix grammar issue in long package description.

 -- Axel Beckert <abe@debian.org>  Wed, 29 Jul 2015 00:00:47 +0200

libtext-markup-perl (0.23-1) unstable; urgency=low

  * Initial Release. (Closes: #792077)

 -- Axel Beckert <abe@debian.org>  Sat, 11 Jul 2015 04:36:23 +0200
